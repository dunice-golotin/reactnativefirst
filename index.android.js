
import React, { Component } from 'react';
import { AppRegistry, } from 'react-native';
import Router from './project/router';
import { Provider } from 'react-redux';
import configureStore from './project/configureStore';
const store = configureStore();
/*class NoAwesomeProject extends Component {
  render() {
    return (
        <Router/>
    );
  }
}*/
const NoAwesomeProject = () => (
  <Provider store={store}>
    <Router />
  </Provider>
)
store.subscribe(()=>{
  console.log('smdksa',store.getState())
})
AppRegistry.registerComponent('NoAwesomeProject', () => NoAwesomeProject);