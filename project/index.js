import React, { Component } from 'react';
import {
    AppRegistry,
    Text
} from 'react-native';
import { Container, Button } from 'native-base';
import { HeaderComponent } from './components/header';
import { ListComponent,navigateEvent  } from './components/list';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
 class AppComponent extends Component {
    
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderComponent some={navigation}/>,
    });
    constructor(props) {
        super(props);
        this.data = this.props;
        this.props.navigateEvent(this.props.navigation.navigate)
    }
    render() { 
        return (
            <Container>
                <ListComponent />
            </Container>
        );
    }
}
function mapStateToProps(store) {
    return {
        constants: store.constReducer,
        navigateData: store.navigateReducer
    }
}
function mapDispatchToProps(dispatch) {
    return {
        navigateEvent: bindActionCreators(navigateEvent, dispatch)
    }
}


export default connect(
    mapStateToProps, mapDispatchToProps
)(AppComponent)