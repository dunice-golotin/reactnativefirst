import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
} from 'react-native';
import { bindActionCreators } from 'redux'
import { Content, Button, List, Left, ListItem, Right, Body, Icon } from 'native-base';
import { STYLES } from './style';
import { LIST_DATA } from './constants';
import { connect } from 'react-redux'
import { setDataSuccess } from './action';
class ListItemComponent extends Component {
    constructor(props) {
        super(props);
        this.constData=this.props.constData[this.props.semantkey];
    }
    navigateTo() {
        if (this.props.blockData) {
            return;
        }
        this.props.setDataSuccess(true)
        const navigate = this.props.navigateEvent;
        navigate(this.constData.link);
        setTimeout(() => { this.props.setDataSuccess(false) }, 1000)
    }
    render() {
        this.stNavigate = this.props.blockData;
        let itemStyle
        if (this.constData.title === 'PRE-OP EVALUATION') {
            itemStyle = styles.lastItem
        }
        else {
            itemStyle = styles.listItem
        }
        return (
            <ListItem disabled={this.props.blockData} onPress={this.navigateTo.bind(this)} style={itemStyle}>
                <Image style={styles.iconImage} source={{ uri: this.constData.image }} />
                <Body style={styles.bodyListItem}>
                    <Text style={styles.uperText}>{this.constData.title}</Text>
                    <Text style={styles.lowerText}>{this.constData.information}</Text>
                </Body>
                <Right><Icon name='ios-arrow-forward' style={styles.arrowStyle} /></Right>
            </ListItem>
        )
    }
}
const styles = STYLES;

function mapStateToProps(store) {
    return {
        blockData: store.listReducer.blockData,
        constData: store.constReducer,
        navigateEvent:store.navigateReducer.navigate
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setDataSuccess: bindActionCreators(setDataSuccess, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ListItemComponent)
