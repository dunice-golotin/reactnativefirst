export const STYLES = {
  iconImage: {
    width: 55,
    height: 55,
    marginRight: 10,
    marginLeft: 10

  },
  uperText: {
    color: '#0054ff',
    fontWeight: '600',
    fontSize: 21,
    marginBottom: 5
  },
  lowerText: {
    color: '#000',
    fontSize: 17,
  },
  buttonStyle:{
    width: 270, 
    paddingLeft: 67, 
    right: 5, 
    height: 50,
    backgroundColor:'#0054ff'
  },
  buttonText: {
    color: '#909193',
    fontSize: 21,

  },
  textData: {
    marginTop: 20,
    marginLeft: 20,
    fontSize: 20,
    color: '#000'
  },
  miniText: {
    fontSize: 15
  },
  listItem:{
    borderBottomWidth: 2,
     borderBottomColor: '#0054ff' 
  },
  bodyListItem:{
    bottom: 5 
  },
  
  lastItem:{ 
    marginBottom: 20, 
    borderBottomWidth: 0 
  },
  arrowStyle:{
    color:'#000',
    fontSize: 30,
  },
  navigateField: {
    width: 30,
    height: 30,
  }
};