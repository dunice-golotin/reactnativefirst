export const LIST_DATA = [
    {
        title: 'PROCEDURE',
        information: 'Total Knee Arthroplasty',
        image: '../icon/doc.png'
    },
    {
         title: 'RISK PROFILE',
        information: 'ASA Class 3',
        image: '../icon/graph.png'
    },
    {
        title: 'MEDICAL CLEARANCE',
        information: 'Cleared by Cardiologist',
        image: '../icon/device.png'
    },
    {
        title: 'PRE-OP EVALUATION',
        information: 'Testing Comleted',
        image: '../icon/list.png'
    }
]
export const FETCHING_DATA_SUCCESS = 'FETCHING_DATA_SUCCESS'
