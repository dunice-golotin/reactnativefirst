import { FETCHING_DATA_SUCCESS} from './constants'

const initialState = {
  blockData: false,
  
}

export default function listReducer (state = initialState, action) {
  switch (action.type) {
    
    case FETCHING_DATA_SUCCESS:
    var newstate={blockData:action.blockData}
      return {...state,blockData:action.blockData}
      
    
    default:
      return state
  }
}