import React, { Component } from 'react';
import { Header, Left, Body, Icon, Button } from 'native-base';
import {
    Text,
} from 'react-native';
import { STYLES } from './style';
export default class HeaderComponent extends Component {

    render() {

        const {goBack} = this.props.some
        return (
            <Header style={{ backgroundColor: '#0054ff' }}>
                <Left>
                    <Button onPress={()=>goBack()} transparent>
                        <Icon name='menu' style={styles.iconWhite} />
                    </Button>
                </Left>
                <Body>
                    <Text style={styles.container}>READY SURGERY</Text>
                </Body>
            </Header>
        )
    }
}
const styles = STYLES;