import React, { Component } from 'react';
import { Container, Header, Button, Left, Body, Icon, Right, Content, List, ListItem } from 'native-base';
import { ScrollView, Text, View, Image } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { STYLES } from './style';
import { HeaderComponent } from '../header';
import { connect } from 'react-redux'
class ClearanceComponent extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderComponent some={navigation}/>,
    });
    constructor(props){
        super(props);
        this.state = {
            disable: false
        }
    }
    navigte(){
        this.setState({disable:true})
        const {navigate}= this.props.navigation;
        navigate('ClearanceNote');
        setTimeout(()=>{this.setState({disable:false})},2000)
    }
    render() {
        return (
            < ScrollView>
                <List >
                    <ListItem style={styles.headItem}>
                        <Left><Image style={styles.imageStyle} source={{ uri: this.props.constData.image }} /></Left>
                        <Body style={{ marginLeft: -160 }}>
                            <Text style={styles.procedure}>{this.props.constData.title}</Text>
                            <Text style={styles.arthroplasty}>{this.props.constData.information}</Text>
                        </Body>
                    </ListItem>
                    <Text style={styles.title}>   Surgery Information   </Text>
                    <ListItem style={styles.secondItem}>
                        <Grid >
                            <Col >
                                <Text style={styles.styleTextLeft}>BMI 33</Text>
                                <Text style={styles.styleTextLeft}>Hypertension</Text>
                                <Text style={styles.styleTextLeft}>Hypercholesterolemia</Text>
                                <Text style={styles.styleTextLeft}>Hypothyroidism</Text>
                                <Text style={styles.styleTextLeft}>Diabetes</Text>
                                <Text style={styles.styleTextLeft}>Depression</Text>
                                <Text style={styles.styleTextLeft}>CVD</Text>
                                <Text style={styles.styleTextLeft}>CAD</Text>
                                <Text style={styles.styleTextLeft}>MI over 1yr ago</Text>
                                <Text style={styles.styleTextLeft}>Stent over 1yr ago</Text>
                                <Text style={styles.styleTextLeft}>Prior SUrgeries: CABG, Tubal Ligation</Text>
                            </Col>
                        </Grid>
                    </ListItem>
                    <Text style={styles.title}>   Allergies   </Text>
                    <ListItem style={styles.thirdItem}>
                        <Grid >
                            <Col >
                                <Text style={styles.styleTextLeft}>Sulfa</Text>
                                <Text style={styles.styleTextLeft}>Penicillin</Text>
                                <Text style={styles.styleTextLeft}>Erythromycin</Text>
                            </Col>
                        </Grid>
                    </ListItem>
                    <Button disabled={this.state.disable} onPress={this.navigte.bind(this)} style={styles.buttonMessage}>
                        <Text style={styles.textButton}>VIEW CLEARANCE NOTE</Text>
                    </Button>
                </List>

            </ ScrollView>

        )
    }
}
const styles = STYLES;
function mapStateToProps(store) {
    return {
        constData: store.constReducer.medical
    }
}

export default connect(
    mapStateToProps
)(ClearanceComponent)