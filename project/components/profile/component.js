import React, { Component } from 'react';
import { Container, Header, Button, Left, Body, Icon, Right, Content, List, ListItem } from 'native-base';
import {  ScrollView, Text, View, Image } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { STYLES } from './style';
import { HeaderComponent } from '../header';
export default class ProfileComponent extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderComponent some={navigation}/>,
    });
    constructor(props){
        super(props);

    }
    render() {
        return (
            < ScrollView>
                <List >
                    <ListItem style={styles.headItem}>
                        <Left><Image style={styles.imageStyle} source={{ uri: 'http://s018.radikal.ru/i501/1705/3a/5445b84bb2ec.png' }} /></Left>
                        <Body style={{ marginLeft: -160 }}>
                            <Text style={styles.procedure}>RISK PROFILE</Text>
                            <Text style={styles.arthroplasty}>ASA CLASS 3</Text>
                        </Body>
                    </ListItem>
                    <Text style={styles.title}>    Composite Episode Risk   </Text>
                    <ListItem style={styles.secondItem}>
                        <Image style={styles.graphStyle} source={{uri:'http://s019.radikal.ru/i602/1705/50/10ed0b7b2a2e.png'}}/>
                    </ListItem>
                    <Text style={styles.title}>   Risks by Dimension   </Text>
                    <ListItem style={styles.thirdItem}>
                        <Grid >
                            <Col >
                                <Text style={styles.styleTextLeft}>Infection</Text>
                                <Text style={styles.styleTextLeft}>Blood Clot</Text>
                                <Text style={styles.styleTextLeft}>Comorbidity</Text>
                                <Text style={styles.styleTextLeft}>Anesthesia</Text>
                                <Text style={styles.styleTextLeft}>Readmission</Text>
                            </Col>
                            <Col >
                                <Text style={styles.styleTextRight}>Medium</Text>
                                <Text style={styles.styleTextRight}>High</Text>
                                <Text style={styles.styleTextRight}>High</Text>
                                <Text style={styles.styleTextRight}>ASA Class 3</Text>
                                <Text style={styles.styleTextRight}>Comming Soon</Text>
                            </Col>
                        </Grid>
                    </ListItem>
                    
                </List>

            </ ScrollView>

        )
    }
}
const styles = STYLES;