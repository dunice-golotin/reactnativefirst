export const STYLES = {

    styleTextRight: {
        textAlign: 'right',
        fontSize: 16,
        color: '#000'
    },
    styleTextLeft: {
        textAlign: 'left',
        fontSize: 16,
        color: '#000'
    },
    headItem: {
        borderBottomWidth: 0,
        top: -10,
    },
    procedure: {
        marginTop: 8,
        fontSize: 22,
        fontWeight: '400',
        color: '#0054ff',
        marginBottom: 10,
    },
    imageStyle: {
        width: 67,
        height: 67,
        top: 15,
    },
    arthroplasty:{
        fontSize:18,
        color: '#000'
    },
    title:{
        fontSize: 20,
        color:'#0054ff',
        fontWeight: '500',
        borderBottomWidth: 2,
        width: 250,
        paddingBottom: 5,
        borderColor:'#0054ff'
    },
    secondItem:{
        borderBottomWidth: 0,
        marginBottom: -14,
    },
    thirdItem:{
        borderBottomWidth: 0,
    },
    buttonMessage:{
        left: 45,
        backgroundColor: '#0054ff'
    },
    textButton:{
        color: '#909193',
        fontSize: 20,
        marginLeft: 20,
        marginRight: 20
    },
    graphStyle:{
        width: 180,
        height: 180,
        marginLeft: 80
    }
};