import React, { Component } from 'react';
import { Container, Header, Button, Left, Body, Icon, Right, Content, List, ListItem } from 'native-base';
import {  ScrollView, Text, View, Image } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { STYLES } from './style';
import { HeaderComponent } from '../header';
import { connect } from 'react-redux'
class ProcedureComponent extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderComponent some={navigation}/>,
    });
    render() {
        return (
            < ScrollView>
                <List >
                    <ListItem style={styles.headItem}>
                        <Left><Image style={styles.imageStyle} source={{ uri: this.props.constData.image }} /></Left>
                        <Body style={{ marginLeft: -160 }}>
                            <Text style={styles.procedure}>{this.props.constData.title}</Text>
                            <Text style={styles.arthroplasty}>{this.props.constData.information}</Text>
                        </Body>
                    </ListItem>
                    <Text style={styles.title}>   Surgery Information   </Text>
                    <ListItem style={styles.secondItem}>
                        <Grid >
                            <Col >
                                <Text style={styles.styleTextLeft}>Date</Text>
                                <Text style={styles.styleTextLeft}>Surgeon</Text>
                                <Text style={styles.styleTextLeft}>Duration</Text>
                                <Text style={styles.styleTextLeft}>Invasiveness</Text>
                                <Text style={styles.styleTextLeft}>Status</Text>
                                <Text style={styles.styleTextLeft}>Position</Text>
                                <Text style={styles.styleTextLeft}>Laterality</Text>

                            </Col>
                            <Col >
                                <Text style={styles.styleTextRight}>Wed 05/17/17</Text>
                                <Text style={styles.styleTextRight}>Jaideep lyengar</Text>
                                <Text style={styles.styleTextRight}>2hrs</Text>
                                <Text style={styles.styleTextRight}>Open</Text>
                                <Text style={styles.styleTextRight}>Inpatient</Text>
                                <Text style={styles.styleTextRight}>Supine</Text>
                                <Text style={styles.styleTextRight}>Left</Text>
                            </Col>
                        </Grid>
                    </ListItem>
                    <Text style={styles.title}>   Anesthesia Requests   </Text>
                    <ListItem style={styles.thirdItem}>
                        <Grid >
                            <Col >
                                <Text style={styles.styleTextLeft}>Date</Text>
                                <Text style={styles.styleTextLeft}>Surgeon</Text>
                                <Text style={styles.styleTextLeft}>Duration</Text>
                                <Text style={styles.styleTextLeft}>Invasiveness</Text>
                            </Col>
                            <Col >
                                <Text style={styles.styleTextRight}>Wed 05/17/17</Text>
                                <Text style={styles.styleTextRight}>Jaideep lyengar</Text>
                                <Text style={styles.styleTextRight}>2hrs</Text>
                                <Text style={styles.styleTextRight}>Open</Text>
                            </Col>
                        </Grid>
                    </ListItem>
                    <Button style={styles.buttonMessage}>
                        <Text style={styles.textButton}>MESSAGE SURGEON
                    </Text>
                    </Button>
                </List>

            </ ScrollView>

        )
    }
}
const styles = STYLES;
function mapStateToProps(store) {
    return {
        constData: store.constReducer.procedure
    }
}

export default connect(
    mapStateToProps
)(ProcedureComponent)