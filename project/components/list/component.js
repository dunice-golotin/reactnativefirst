import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    ScrollView
} from 'react-native';
import { Button, List, ListItem, Right, Body, Icon } from 'native-base';
import { STYLES } from './style';
import { LIST_DATA } from './constants';
import { ListItemComponent } from '../listitem';

class ListComponent extends Component {
    constructor(props){
        super(props);
        
    }
    render() {
        let obj = Object.keys(this.props.constants)
        let template = obj.map(function (item, index) {
            return (
                <ListItemComponent key={index}  semantkey={item} />
            )
        })
        return (

            < ScrollView>
                <Text style={styles.textData}>May 17<Text style={styles.miniText}>TH</Text> 10:00<Text style={styles.miniText}>AM</Text> Case</Text>
                <List style={{ marginLeft: -17 }}>
                    {template}
                </List>
                <Body>
                    <Button style={styles.buttonStyle}>
                        <Text style={styles.buttonText}>CALL PATIENT</Text>
                    </Button>
                </Body>
            </ ScrollView>
        )
    }
}

const styles = STYLES;
function mapStateToProps(store) {
    return {
        constants: store.constReducer,
    }
}



export default connect(
    mapStateToProps, 
)(ListComponent)