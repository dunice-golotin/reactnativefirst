export function navigateEvent(data) {
  return {
    type: 'NAVIGATE',
    navigate: data
  }
}
