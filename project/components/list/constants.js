export  const LIST_DATA = [
    {
        title: 'PROCEDURE',
        information: 'Total Knee Arthroplasty',
        image: 'http://s015.radikal.ru/i332/1705/40/c11969d4e41f.png',
        link:'Procedure'
    },
    {
         title: 'RISK PROFILE',
        information: 'ASA Class 3',
        image: 'http://s018.radikal.ru/i501/1705/3a/5445b84bb2ec.png',
        link:'Profile'
    },
    {
        title: 'MEDICAL CLEARANCE',
        information: 'Cleared by Cardiologist',
        image: 'http://s018.radikal.ru/i513/1705/5e/d069f6e0dd55.png',
        link:'Clearance'
    },
    {
        title: 'PRE-OP EVALUATION',
        information: 'Testing Comleted',
        image: 'http://s41.radikal.ru/i091/1705/93/fb0dcefdaf86.png',
        link:'Evaluation'
    },
    
    
]

/*
[url=http://radikal.ru][img]ttp:/h/s018.radikal.ru/i513/1705/5e/d069f6e0dd55.png[/img][/url]

[url=http://radikal.ru][img]http://s015.radikal.ru/i332/1705/40/c11969d4e41f.png[/img][/url]

[url=http://radikal.ru][img]http://s018.radikal.ru/i501/1705/3a/5445b84bb2ec.png[/img][/url]

[url=http://radikal.ru][img]http://s41.radikal.ru/i091/1705/93/fb0dcefdaf86.png[/img][/url]


 */