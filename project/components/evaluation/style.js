export const STYLES = {

    styleTextRight: {
        textAlign: 'right',
        fontSize: 18,
        color: '#90d940'
    },
    styleTextLeft: {
        textAlign: 'left',
        fontSize: 18,
        color: '#000'
    },
    headItem: {
        borderBottomWidth: 0,
        top: -10,
    },
    procedure: {
        marginTop: 8,
        fontSize: 22,
        fontWeight: '400',
        color: '#0054ff',
        marginBottom: 10,
    },
    imageStyle: {
        width: 67,
        height: 67,
        top: 15,
    },
    arthroplasty:{
        fontSize:18,
        color: '#000'
    },
    title:{
        fontSize: 20,
        color:'#0054ff',
        fontWeight: '500',
        borderBottomWidth: 2,
        width: 230,
        paddingBottom: 5,
        borderColor:'#0054ff'
    },
    secondItem:{
        borderBottomWidth: 0,
        marginBottom: 30,
    },
    thirdItem:{

        borderBottomWidth: 0,
    },
    styleTextLink: {
        textAlign: 'right',
        fontSize: 18,
        color: '#0054ff'
    },
    
    
};