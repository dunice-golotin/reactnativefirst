import React, { Component } from 'react';
import { Container, Header, Button, Left, Body, Icon, Right, Content, List, ListItem } from 'native-base';
import { Text, View, Image,WebView, Linking,  ScrollView } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { STYLES } from './style';
import { HeaderComponent } from '../header';
import { connect } from 'react-redux'
class EvaluationComponent extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderComponent some={navigation}/>,
    });
    constructor(props) {
        super(props);

    }
    goAway(){
        Linking.openURL('https://s3-us-west-2.amazonaws.com/readysurgeryuploads/s3-website-us-west-2.amazonaws.com/images/zdenekekg.png').catch(err => console.error('An error occurred', err));
    }
    render() {
        return (
            < ScrollView>
                <List >
                    <ListItem style={styles.headItem}>
                        <Left><Image style={styles.imageStyle} source={{ uri: this.props.constData.image }} /></Left>
                        <Body style={{ marginLeft: -160 }}>
                            <Text style={styles.procedure}>{this.props.constData.title}</Text>
                            <Text style={styles.arthroplasty}>{this.props.constData.information}</Text>
                        </Body>
                    </ListItem>
                    <Text style={styles.title}>    Required Diagnostics   </Text>
                    <ListItem style={styles.secondItem}>
                        <Grid >
                            <Col >
                                <Text style={styles.styleTextLeft}>WBC</Text>
                                <Text style={styles.styleTextLeft}>Hemoglobin</Text>
                                <Text style={styles.styleTextLeft}>Hematocrit</Text>
                                <Text style={styles.styleTextLeft}>Platelets</Text>
                                <Text style={styles.styleTextLeft}>Creatinine</Text>
                                <Text style={styles.styleTextLeft}>GFR</Text>
                                <Text style={styles.styleTextLeft}>HgB A1C</Text>
                                <Text style={styles.styleTextLeft}>EKG</Text>
                            </Col>
                            <Col >
                                <Text style={styles.styleTextRight}>8.1</Text>
                                <Text style={styles.styleTextRight}>12.1</Text>
                                <Text style={styles.styleTextRight}>37.6</Text>
                                <Text style={styles.styleTextRight}>262</Text>
                                <Text style={styles.styleTextRight}>0.75</Text>
                                <Text style={styles.styleTextRight}>91</Text>
                                <Text style={styles.styleTextRight}>6.3</Text>
                                <Text style={styles.styleTextLink} onPress={this.goAway.bind(this)}>View</Text>
                            </Col>
                        </Grid>
                    </ListItem>
                    <Text style={styles.title}>   Anesthesia Precautions   </Text>
                    <ListItem style={styles.thirdItem}>
                        <Text style={styles.styleTextLeft}>Anxiety</Text>
                    </ListItem>
                </List>

            </ ScrollView>

        )
    }
}
const styles = STYLES;
function mapStateToProps(store) {
    return {
        constData: store.constReducer.preOp
    }
}

export default connect(
    mapStateToProps
)(EvaluationComponent)