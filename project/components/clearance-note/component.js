import React, { Component } from 'react';
import { Container, Header, Button, Left, Body, Icon, Right, Content, List, ListItem } from 'native-base';
import {  ScrollView, Text, View, Image } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { STYLES } from './style';
import { HeaderComponent } from '../header';
import { connect } from 'react-redux'
class ClearanceNoteComponent extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <HeaderComponent some={navigation}/>,
    });
    render() {
        return (
            < ScrollView>
                <List >
                    <ListItem style={styles.headItem}>
                        <Left><Image style={styles.imageStyle} source={{ uri: this.props.constData.image }} /></Left>
                        <Body style={{ marginLeft: -160 }}>
                            <Text style={styles.procedure}>{this.props.constData.title}</Text>
                            <Text style={styles.arthroplasty}>{this.props.constData.information}</Text>
                        </Body>
                    </ListItem>
                    <Text style={styles.title}>Clearance Note</Text>
                    <ListItem style={styles.secondItem}>
                        <Text style={styles.largeText}>Perform ischemia update via pharmacological-MPI
                        stress test given less then 4 METS functional capacity.
                        If her MPI is normal, she may proceed with elective knee
                        surgery with low perioperative cardiac risk. If not   normal,
                        then she will postpone surgery and see me sooner then
                        requested for further evaluation.
                        </Text>
                    </ListItem>
                    <ListItem  style={styles.thirdItem}>
                        <Text style={styles.largeText}>Stress Test Results:</Text>
                    </ListItem>
                    <ListItem  style={styles.thirdItem}>
                        <Text style={styles.largeText}>1. Post-stress global LV EF 79%</Text>
                    </ListItem>
                    <ListItem  style={styles.thirdItem}>
                        <Text style={styles.largeText}>2. No stress perfusion defects seen</Text>
                    </ListItem>
                    <ListItem  style={styles.thirdItem}>
                        <Text style={styles.largeText}>3. No evidence for infarct</Text>
                    </ListItem>

                    <Button style={styles.buttonMessage}>
                        <Text style={styles.textButton}>MESSAGE CARDIOLOGIST</Text>
                    </Button>
                </List>

            </ ScrollView>

        )
    }
}
const styles = STYLES;
function mapStateToProps(store) {
    return {
        constData: store.constReducer.medical
    }
}

export default connect(
    mapStateToProps
)(ClearanceNoteComponent)