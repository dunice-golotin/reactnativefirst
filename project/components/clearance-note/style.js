export const STYLES = {
    styleTextLeft: {
        textAlign: 'left',
        fontSize: 15,
        color: '#000'
    },
    headItem: {
        borderBottomWidth: 0,
        top: -10,
        marginBottom: -10
    },
    procedure: {
        marginTop: 8,
        fontSize: 22,
        fontWeight: '400',
        color: '#0054ff',
        marginBottom: 10,
    },
    imageStyle: {
        width: 60,
        height: 60,
        top: 10,
    },
    arthroplasty:{
        fontSize:18,
        color: '#000'
    },
    title:{
        paddingLeft: 25,
        fontSize: 20,
        color:'#0054ff',
        fontWeight: '500',
        borderBottomWidth: 2,
        width: 250,
        paddingBottom: 5,
        borderColor:'#0054ff',
        marginBottom: -15
    },
    secondItem:{
        borderBottomWidth: 0,
        marginBottom: -14,
    },
    thirdItem:{
        borderBottomWidth: 0,
        marginBottom: -18,
        paddingTop: 3
    },
    buttonMessage:{
        left: 35,
        backgroundColor: '#0054ff',
        marginTop: 30,
        height: 60
    },
    textButton:{
        color: '#909193',
        fontSize: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    largeText: {
        fontSize: 17,
        color: '#000',
        width: 300,
        marginLeft: 10
    }
};