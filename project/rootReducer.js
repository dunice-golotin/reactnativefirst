import { combineReducers } from 'redux'
import { listReducer } from './components/listitem'
import { constReducer } from './constReducer';
import { navigateReducer } from './components/list'
const rootReducer = combineReducers({
    listReducer,
    constReducer,
    navigateReducer 
})

export default rootReducer