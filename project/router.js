import { StackNavigator } from 'react-navigation';
import React, { Component } from 'react';
import App from './index';
import { ProcedureComponent } from './components/procedure';
import { ProfileComponent } from './components/profile';
import { ClearanceComponent } from './components/clearance';
import { ClearanceNoteComponent } from './components/clearance-note';
import { EvalueationComponent } from './components/evaluation'

export default  Router = StackNavigator({
  Home: { screen: App },
  Procedure: { screen: ProcedureComponent },
  Profile: { screen: ProfileComponent },
  Clearance: { screen: ClearanceComponent },
  ClearanceNote: { screen: ClearanceNoteComponent },
  Evaluation: { screen: EvalueationComponent }
});