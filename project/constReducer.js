const initialState = {
    procedure: {
        title: 'PROCEDURE',
        information: 'Total Knee Arthroplasty',
        image: 'http://s015.radikal.ru/i332/1705/40/c11969d4e41f.png',
        link: 'Procedure'
    },
    risk:
    {
        title: 'RISK PROFILE',
        information: 'ASA Class 3',
        image: 'http://s018.radikal.ru/i501/1705/3a/5445b84bb2ec.png',
        link: 'Profile'
    },
    medical:
    {
        title: 'MEDICAL CLEARANCE',
        information: 'Cleared by Cardiologist',
        image: 'http://s018.radikal.ru/i513/1705/5e/d069f6e0dd55.png',
        link: 'Clearance'
    },
    preOp:
    {
        title: 'PRE-OP EVALUATION',
        information: 'Testing Completed',
        image: 'http://s41.radikal.ru/i091/1705/93/fb0dcefdaf86.png',
        link: 'Evaluation'
    },
}
export  function constReducer (state = initialState, action) {
    switch (action.type) {

        case 'CHENGE_PROCEDURE':
            return { ...state, procedure: action.payload }
        default:
            return state;
    }

}